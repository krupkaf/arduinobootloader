# Faster and smaller bootloader for Aurduino

ARDUINO compatible bootloader

## Tested on:
* AVRCD with ATMega324pa @ 20MHz    https://gitlab.com/krupkaf/avrcd
* Arduino UNO with ATMega328p @ 16 MHz
* Arduino NANO with ATMega328p @ 16 MHz
