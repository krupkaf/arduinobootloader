GCCDIR    = /opt/arduino-1.8.10/hardware/tools/avr/bin
CC        = $(GCCDIR)/avr-gcc
OBJCOPY   = $(GCCDIR)/avr-objcopy
OBJDUMP   = $(GCCDIR)/avr-objdump
SIZE      = $(GCCDIR)/avr-size
AVRDUDE   = $(GCCDIR)/avrdude
OBJDIR    = ./build/
AVRDUDE_CONFIG = ../../lib/AVR/avrdude.conf
# ISPPORT   = /dev/avr910
ISPPORT   = /dev/ttyUSB0

MMCU      =
F_CPU     =
BAUD_RATE = 115200
BOARD     =
RS485RW		=

all:

AVRCD_m324pa_20MHz: MMCU=atmega324pa
AVRCD_m324pa_20MHz: F_CPU=20000000L
AVRCD_m324pa_20MHz: SECTION_START=0x7C00
AVRCD_m324pa_20MHz: LED = -DLED_DDR=DDRB -DLED_PORT=PORTB -DLED=PINB0
AVRCD_m324pa_20MHz: BAUD_RATE = 38400
AVRCD_m324pa_20MHz: BOARD = AVRCD_m324pa
AVRCD_m324pa_20MHz: common

AVRCD_m324pa_20MHz_flash: MMCU=atmega324pa
AVRCD_m324pa_20MHz_flash: F_CPU=20000000L
AVRCD_m324pa_20MHz_flash: LFUSE = FF
AVRCD_m324pa_20MHz_flash: HFUSE = D4
AVRCD_m324pa_20MHz_flash: EFUSE = FC
AVRCD_m324pa_20MHz_flash: BOARD = AVRCD_m324pa
AVRCD_m324pa_20MHz_flash: flash

AVRCD_m644p_20MHz: MMCU=atmega644p
AVRCD_m644p_20MHz: F_CPU=20000000L
AVRCD_m644p_20MHz: SECTION_START=0xFC00
AVRCD_m644p_20MHz: LED = -DLED_DDR=DDRB -DLED_PORT=PORTB -DLED=PINB0
AVRCD_m644p_20MHz: BAUD_RATE = 38400
AVRCD_m644p_20MHz: BOARD = AVRCD_m644p
AVRCD_m644p_20MHz: common

AVRCD_m644p_20MHz_flash: MMCU=atmega644p
AVRCD_m644p_20MHz_flash: F_CPU=20000000L
AVRCD_m644p_20MHz_flash: LFUSE = FF
AVRCD_m644p_20MHz_flash: HFUSE = D6
AVRCD_m644p_20MHz_flash: EFUSE = FC
AVRCD_m644p_20MHz_flash: BOARD = AVRCD_m644p
AVRCD_m644p_20MHz_flash: flash

AVRCD_m1284p_20MHz: MMCU=atmega1284p
AVRCD_m1284p_20MHz: F_CPU=20000000L
AVRCD_m1284p_20MHz: SECTION_START=0x01FC00
AVRCD_m1284p_20MHz: LED = -DLED_DDR=DDRB -DLED_PORT=PORTB -DLED=PINB0
AVRCD_m1284p_20MHz: BAUD_RATE = 115200
AVRCD_m1284p_20MHz: BOARD = AVRCD_m1284p
AVRCD_m1284p_20MHz: common

AVRCD_m1284p_20MHz_flash: MMCU=atmega1284p
AVRCD_m1284p_20MHz_flash: F_CPU=20000000L
AVRCD_m1284p_20MHz_flash: LFUSE = FF
AVRCD_m1284p_20MHz_flash: HFUSE = D6
AVRCD_m1284p_20MHz_flash: EFUSE = FC
AVRCD_m1284p_20MHz_flash: BOARD = AVRCD_m1284p
AVRCD_m1284p_20MHz_flash: flash

UNO_m328p_16MHz: MMCU=atmega328p
UNO_m328p_16MHz: F_CPU=16000000L
UNO_m328p_16MHz: SECTION_START=0x7C00
UNO_m328p_16MHz: LED = -DLED_DDR=DDRB -DLED_PORT=PORTB -DLED=PINB5
UNO_m328p_16MHz: BAUD_RATE = 115200
UNO_m328p_16MHz: BOARD = UNO_m328p_16MHz
UNO_m328p_16MHz: common

UNO_m328p_16MHz_flash: MMCU=atmega328p
UNO_m328p_16MHz_flash: F_CPU=16000000L
UNO_m328p_16MHz_flash: LFUSE = FF
UNO_m328p_16MHz_flash: HFUSE = D4
UNO_m328p_16MHz_flash: EFUSE = 04
UNO_m328p_16MHz_flash: BOARD = UNO_m328p_16MHz
UNO_m328p_16MHz_flash: flash

NANO_m328p_16MHz_flash: UNO_m328p_16MHz_flash

FARMLAB: MMCU=atmega328p
FARMLAB: F_CPU=14745600L
FARMLAB: SECTION_START=0x7C00
# FARMLAB: LED = -DLED_DDR=DDRB -DLED_PORT=PORTB -DLED=PINB5 -DNUM_LED_FLASHES=20
FARMLAB: RS485RW = -DRS485RW_DDR=DDRD -DRS485RW_PORT=PORTD -DRS485RW=PIND2
FARMLAB: BAUD_RATE = 9600
FARMLAB: BOARD = FARMLAB
FARMLAB: common

FARMLAB_flash: MMCU=atmega328p
FARMLAB_flash: F_CPU=14745600L
FARMLAB_flash: LFUSE = BF
FARMLAB_flash: HFUSE = D4
FARMLAB_flash: EFUSE = 04
FARMLAB_flash: BOARD = FARMLAB
FARMLAB_flash: flash

#mozno pouzit i pro atmega168a
SensorA160608_M168: MMCU=atmega168
SensorA160608_M168: F_CPU=3686400L
SensorA160608_M168: SECTION_START=0x3C00
SensorA160608_M168: LED = -DLED_DDR=DDRB -DLED_PORT=PORTB -DLED=PINB0 -DNUM_LED_FLASHES=20
SensorA160608_M168: RS485RW = -DRS485RW_DDR=DDRD -DRS485RW_PORT=PORTD -DRS485RW=PIND2
SensorA160608_M168: BAUD_RATE = 9600
SensorA160608_M168: BOARD = SensorA160608_M168
SensorA160608_M168: common

SensorA160608_M168_flash: MMCU=atmega168
SensorA160608_M168_flash: F_CPU=3686400L
SensorA160608_M168_flash: LFUSE = FF
SensorA160608_M168_flash: HFUSE = D4
SensorA160608_M168_flash: EFUSE = 02
SensorA160608_M168_flash: BOARD = SensorA160608_M168
SensorA160608_M168_flash: flash

common:
	@echo $(BOARD) $(MMCU) $(F_CPU)
	@mkdir	-p $(OBJDIR)
	@echo cc ATmegaBOOT.c
	@$(CC)	-g -Wall -Os -ffreestanding -ffunction-sections $(LED) $(RS485RW) -DBOARD=$(BOARD) -DBAUD_RATE=$(BAUD_RATE) -mmcu=$(MMCU) -DF_CPU=$(F_CPU) -c -o $(OBJDIR)BOOT_$(BOARD)_$(MMCU)_$(F_CPU).o ATmegaBOOT.c
	@echo link
	@$(CC) -nostartfiles -g -Wall -Os -mmcu=$(MMCU) -DF_CPU=$(F_CPU) -Wl,--relax,--gc-sections,--section-start=.text=$(SECTION_START) -o $(OBJDIR)BOOT_$(BOARD)_$(MMCU)_$(F_CPU).elf $(OBJDIR)BOOT_$(BOARD)_$(MMCU)_$(F_CPU).o
	@echo objcopy
	@$(OBJCOPY) -O ihex $(OBJDIR)BOOT_$(BOARD)_$(MMCU)_$(F_CPU).elf $(OBJDIR)BOOT_$(BOARD)_$(MMCU)_$(F_CPU).hex
	@echo objdump
	@$(OBJDUMP) -S $(OBJDIR)BOOT_$(BOARD)_$(MMCU)_$(F_CPU).elf > $(OBJDIR)BOOT_$(BOARD)_$(MMCU)_$(F_CPU).lst
	@echo size
	@$(SIZE) -C --mcu=$(MMCU) $(OBJDIR)BOOT_$(BOARD)_$(MMCU)_$(F_CPU).elf
	@echo

flash:
	$(AVRDUDE) -C $(AVRDUDE_CONFIG) -c avr910 -p $(MMCU) -P $(ISPPORT) -b 115200 -e -u -U efuse:w:0x$(EFUSE):m -U hfuse:w:0x$(HFUSE):m -U lfuse:w:0x$(LFUSE):m
	$(AVRDUDE) -C $(AVRDUDE_CONFIG) -c avr910 -p $(MMCU) -P $(ISPPORT) -b 115200 -U flash:w:$(OBJDIR)BOOT_$(BOARD)_$(MMCU)_$(F_CPU).hex -U lock:w:0x0f:m

clean:
	@echo clean
	@rm -rf $(OBJDIR)*.o $(OBJDIR)*.elf $(OBJDIR)*.lst $(OBJDIR)*.map $(OBJDIR)*.sym $(OBJDIR)*.lss $(OBJDIR)*.eep $(OBJDIR)*.srec $(OBJDIR)*.bin
	@echo

cleanAll: clean
	@echo cleanAll
	@rm -rf $(OBJDIR)*.hex
	@echo

readFUSE:
	$(AVRDUDE) -C $(AVRDUDE_CONFIG) -c avr910 -p atmega324p -F -P $(ISPPORT) -b 115200 -e -u -U efuse:r:-:h -U hfuse:r:-:h -U lfuse:r:-:h
