//@see:
// https://github.com/arduino/ArduinoCore-avr/blob/master/bootloaders/atmega/ATmegaBOOT_168.c
// AVR061.pdf
// https://uart.cz/721/bootloader-v-avr/
// https://www.nongnu.org/avr-libc/user-manual/group__avr__boot.html
//
// Licence can be viewed at http://www.fsf.org/licenses/gpl.txt
//
// Max FLASH is 64 KB !
//
// Tested with m324pa
//**********************************************************

#include <avr/boot.h>
#include <avr/eeprom.h>
#include <avr/io.h>
#include <avr/pgmspace.h>
#include <inttypes.h>
#include <util/delay.h>

/* Use the F_CPU defined in Makefile */

/* set the waiting time for the bootloader */
/* get this from the Makefile instead */
#ifndef MAX_TIME_COUNT
#define MAX_TIME_COUNT (F_CPU >> 4)
#endif

/* After this many errors give up and launch application */
#define MAX_ERROR_COUNT 5

#ifndef NUM_LED_FLASHES
#define NUM_LED_FLASHES 5
#endif

/* set the UART baud rate */
#ifndef BAUD_RATE
#define BAUD_RATE 115200
#endif

/* SW_MAJOR and MINOR needs to be updated from time to time to avoid warning
 * message from AVR Studio */
/* never allow AVR Studio to do an update !!!! */
#define HW_VER 0x01
#define SW_MAJOR 0x01
#define SW_MINOR 0x00

/* onboard LED is used to indicate, that the bootloader was entered (flashing)
 */
//#define LED_DDR DDRB    //is defined in makefile
//#define LED_PORT PORTB
//#define LED PINB0

/* function prototypes */
void putch(char);
char getch(void);
void getNch(uint8_t);
void byte_response(uint8_t);
void nothing_response(void);
void countError(void);

void putch_0x14(void) { putch(0x14); }

void putch_0x10(void) { putch(0x10); }

/* some variables */
union address_union {
  uint16_t word;
  uint8_t byte[2];
} address;

union TLengthUnion {
  uint16_t word;
  uint8_t byte[2];
};

uint8_t buff[256];

#define app_start() asm("jmp 0x000\n")

void main(void) __attribute__((naked)) __attribute__((section(".init0")));

uint8_t error_count;

/* main program starts here */
void main(void) {
  uint8_t ch;

  asm("eor	r1, r1");
  SREG = 0;
  SP = RAMEND;

  MCUSR &= ~(1 << WDRF);
  WDTCSR |= (1 << WDCE) | (1 << WDE);
  WDTCSR = 0x00;

  error_count = MAX_ERROR_COUNT;

  /* initialize UART(s) depending on CPU defined */
  UBRR0L = (uint8_t)((float)F_CPU / ((float)BAUD_RATE * 8.0) - 1.0 + 0.5);
  UBRR0H =
      ((uint8_t)((float)F_CPU / ((float)BAUD_RATE * 8.0) - 1.0 + 0.5)) >> 8;
  UCSR0A = _BV(U2X0);
  UCSR0C = 0x06;
  UCSR0B = _BV(TXEN0) | _BV(RXEN0);
  DDRD &= ~_BV(PIND0);
  PORTD &= ~_BV(PIND0);
  DDRD |= _BV(PIND1);
  PORTD |= _BV(PIND1);

#ifdef LED
  /* set LED pin as output */
  LED_DDR |= _BV(LED);
  LED_PORT &= ~_BV(LED);
#endif

#ifdef RS485RW
  RS485RW_DDR |= _BV(RS485RW);
  RS485RW_PORT &= ~_BV(RS485RW);
#endif

  /* flash onboard LED to signal entering of bootloader */
  {
    register uint8_t count = 2 * NUM_LED_FLASHES;
    while (count--) {
#ifdef LED
      LED_PORT ^= _BV(LED);
#endif
      _delay_ms(100);
      if (UCSR0A & _BV(RXC0)) {
        break;
      }
    }
  }

  /* forever loop */
  for (;;) {
    ch = getch();
    /* A bunch of if...else if... gives smaller code than switch...case ! */

    /* 0: Hello is anyone home ? */
    /* P: Enter programming mode  */
    /* R: Erase device, don't care as we will erase one page at a time anyway.*/
    /* Q: Leave programming mode  */

    if (ch == '0' || ch == 'P' || ch == 'R' || ch == 'Q') {
      nothing_response();
    }

    /* Request programmer ID */
    else if (ch == '1') {
      if (getch() == ' ') {
        putch_0x14();
        putch('A');
        putch('V');
        putch('R');
        putch(' ');
        putch('I');
        putch('S');
        putch('P');
        putch_0x10();
      } else {
        countError();
      }
    }

    /* AVR ISP/STK500 board commands  DON'T CARE so default nothing_response */
    else if (ch == '@') {
      if (getch() > 0x85) {
        getch();
      }
      nothing_response();
    }

    /* 0x41 Get Parameter Value*/
    else if (ch == 'A') {
      ch = getch();
      if (ch == 0x80)
        byte_response(HW_VER); // Hardware version
      else if (ch == 0x81)
        byte_response(SW_MAJOR); // Software major version
      else if (ch == 0x82)
        byte_response(SW_MINOR); // Software minor version
      else if (ch == 0x98)
        byte_response(
            0x03); // Unknown but seems to be required by avr studio 3.56
      else
        byte_response(
            0x00); // Covers various unnecessary responses we don't care about
    }

    /* Device Parameters  DON'T CARE, DEVICE IS FIXED  */
    else if (ch == 'B') {
      getNch(20);
      nothing_response();
    }

    /* Parallel programming stuff  DON'T CARE  */
    else if (ch == 'E') {
      getNch(4);
      nothing_response();
    }

    /* 0x55 Load Address */
    /* Set address, little endian. EEPROM in bytes, FLASH in words  */
    /* Perhaps extra address bytes may be added in future to support > 128kB
       FLASH.  */
    /* This might explain why little endian was used here, big endian used
       everywhere else.  */
    else if (ch == 'U') {
      address.byte[0] = getch();
      address.byte[1] = getch();
      address.word <<= 1;
      nothing_response();
    }

    /* Universal SPI programming command, disabled.  Would be used for fuses and
       lock bits.  */
    else if (ch == 'V') {
      if (getch() == 0x30) {
        getch();
        ch = getch();
        getch();
        if (ch == 0) {
          byte_response(SIGNATURE_0);
        } else if (ch == 1) {
          byte_response(SIGNATURE_1);
        } else {
          byte_response(SIGNATURE_2);
        }
      } else {
        getNch(3);
        byte_response(0x00);
      }
    }

    /* Write memory, length is big endian and is in bytes  */
    else if (ch == 'd') {
      union length_union {
        uint16_t word;
        uint8_t byte[2];
      } length;
      length.byte[1] = getch();
      length.byte[0] = getch();
      char memtype = getch();
      for (uint16_t w = 0; w < length.word; w++) {
        buff[w] = getch(); // Store data in buffer, can't keep up with serial
                           // data stream whilst programming pages
      }
      if (getch() == ' ') {
        if (memtype == 'E') { // Write to EEPROM one byte at a time
          for (uint16_t w = 0; w < length.word; w++) {
            eeprom_write_byte((void *)address.word, buff[w]);
            address.word++;
          }
        } else { // Write to FLASH one page at a time
          uint8_t *buf = buff;
          if ((length.byte[0] & 0x01))
            length.word++;    // Even up an odd number of bytes
          eeprom_busy_wait(); // Wait for previous EEPROM writes to complete

          for (; length.word > 0;) {
            uint16_t page = address.word;
            boot_page_erase(page);
            boot_spm_busy_wait(); // Wait until the memory is erased.
            for (uint8_t i = 0; ((i < (SPM_PAGESIZE / 2)) && (length.word > 0));
                 i += 1) {
              // Set up little-endian word.
              uint16_t w = *buf++;
              w += (*buf++) << 8;
              boot_page_fill(address.word, w);
              address.word += 2;
              length.word -= 2;
            }
            boot_page_write(page); // Store buffer in flash page.
            boot_spm_busy_wait();  // Wait until the memory is written.
            // Reenable RWW-section again. We need this if we want to jump back
            // to the application after bootloading.
            boot_rww_enable();
          }
        }
        putch_0x14();
        putch_0x10();
      } else {
        countError();
      }
    }

    /* 0x74 Read Page */
    else if (ch == 't') {
      union TLengthUnion length;
      length.byte[1] = getch();
      length.byte[0] = getch();
      char memtype = getch();
      if (getch() == ' ') { // Command terminator
        putch_0x14();
        for (uint16_t w = length.word; w;
             w--) {             // Can handle odd and even lengths okay
          if (memtype == 'E') { // Byte access EEPROM read
            putch(eeprom_read_byte((void *)address.word));
          } else {
            putch(pgm_read_byte_near(address.word));
          }
          address.word++;
        }
        putch_0x10();
      }
    }

    /* Get device signature bytes  */
    else if (ch == 'u') {
      if (getch() == ' ') {
        putch_0x14();
        putch(SIGNATURE_0);
        putch(SIGNATURE_1);
        putch(SIGNATURE_2);
        putch_0x10();
      } else {
        countError();
      }
    }

    /* Read oscillator calibration byte */
    else if (ch == 'v') {
      byte_response(0x00);
    } else {
      countError();
    }
  } /* end of forever loop */
}

void putch(char ch) {
#ifdef RS485RW
  RS485RW_PORT |= _BV(RS485RW);
#endif
  while (!(UCSR0A & _BV(UDRE0)))
    ;
  UDR0 = ch;
#ifdef RS485RW
  UCSR0A |= _BV(TXC0);
  while (!(UCSR0A & _BV(TXC0)))
    ;
  RS485RW_PORT &= ~_BV(RS485RW);
#endif
}

char getch(void) {
  uint32_t count = MAX_TIME_COUNT;
  while (!(UCSR0A & _BV(RXC0))) {
    if (--count == 0) {
      app_start();
    }
  }
  return UDR0;
}

void getNch(uint8_t count) {
  while (count--) {
    getch(); // need to handle time out
  }
}

void byte_response(uint8_t val) {
  if (getch() == ' ') {
    putch_0x14();
    putch(val);
    putch_0x10();
  } else {
    countError();
  }
}

void nothing_response(void) {
  if (getch() == ' ') {
    putch_0x14();
    putch_0x10();
  } else {
    countError();
  }
}

void countError(void) {
  if (--error_count == 0) {
    app_start();
  }
}
